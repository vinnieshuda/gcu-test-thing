// Fill out your copyright notice in the Description page of Project Settings.

#include "GCUBaseCharacter.h"


// Sets default values
AGCUBaseCharacter::AGCUBaseCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGCUBaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGCUBaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AGCUBaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

