// Fill out your copyright notice in the Description page of Project Settings.

#include "GCUCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/GrapplingHook/GrapplingHookComponent.h"
#include "Kismet/KismetMathLibrary.h"

AGCUCharacter::AGCUCharacter()
{
	// Create default components
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera Component"));
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("Spring Arm"));
	GrapplingHook = CreateDefaultSubobject<UGrapplingHookComponent>(TEXT("Physics Constraint"));

	SpringArm->SetupAttachment(RootComponent);
	CameraComponent->SetupAttachment(SpringArm);

	SpringArm->bUsePawnControlRotation = true;
	SpringArm->TargetArmLength = 400.f;

	GrapplingHook->SetupAttachment(RootComponent);

	// Invert mouse
	bInvertMouseEnabled = false;
	SuperJumpPowerMultiplier = 100.f;
}

void AGCUCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Check SuperJump and space keypress.. if Space is released we should do normal jump
	if (bSuperJump && !IsActorFalling())
	{
		APlayerController* PC = Cast<APlayerController>(GetController());

		if (PC->IsInputKeyDown(EKeys::SpaceBar))
		{
			SuperJumpPower += DeltaTime * SuperJumpPowerMultiplier;
		}
	}
}

void AGCUCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	if (!PlayerInputComponent)
		return;

	// Axis Binds
	PlayerInputComponent->BindAxis("MoveForwardBack", this, &AGCUCharacter::Input_MoveForwardBack);
	PlayerInputComponent->BindAxis("MoveLeftRight", this, &AGCUCharacter::Input_MoveLeftRight);
	PlayerInputComponent->BindAxis("MousePitch", this, &AGCUCharacter::Input_MousePitch);
	PlayerInputComponent->BindAxis("MouseYaw", this, &AGCUCharacter::Input_MouseYaw);
	PlayerInputComponent->BindAxis("RightMouseAxis", this, &AGCUCharacter::Input_RightMouseAxis);
	// Axis Binds end

	// Action Binds
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AGCUCharacter::Input_JumpPressed);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AGCUCharacter::Input_JumpReleased);
	PlayerInputComponent->BindAction("LeftMouse", IE_Pressed, this, &AGCUCharacter::Input_LeftMousePressed);
	PlayerInputComponent->BindAction("RightMouse", IE_Pressed, this, &AGCUCharacter::Input_RightMousePressed);
	PlayerInputComponent->BindAction("RightMouse", IE_Released, this, &AGCUCharacter::Input_RightMouseReleased);
	// Action Binds end
}


void AGCUCharacter::Input_MoveForwardBack(float inAxisValue)
{
	AddMovementInput(GetActorForwardVector(), inAxisValue);
}

void AGCUCharacter::Input_MoveLeftRight(float inAxisValue)
{
	AddMovementInput(GetActorRightVector(), inAxisValue);
}

void AGCUCharacter::Input_MousePitch(float inAxisValue)
{
	if(bInvertMouseEnabled)
		AddControllerPitchInput(inAxisValue);
	else AddControllerPitchInput(inAxisValue * -1.f);
}

void AGCUCharacter::Input_MouseYaw(float inAxisValue)
{
	AddControllerYawInput(inAxisValue);
}

void AGCUCharacter::Input_JumpPressed()
{
	// We predict jump as superjump always
	bSuperJump = true;
	SuperJumpPower = 0.f;
}

void AGCUCharacter::Input_JumpReleased()
{
	// If our Jump power is under low value we perform normal jump, that means we released space immediatly

	if (SuperJumpPower < 1000.f)
	{
		UE_LOG(LogTemp, Warning, TEXT("Performing Normal Jump"));
		Jump();

		SuperJumpPower = 0.f;
		bSuperJump = false;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Performing Super Jump with Power: %f"), SuperJumpPower);
		Server_ActivateSuperJump(SuperJumpPower);

		SuperJumpPower = 0.f;
		bSuperJump = false;

	}
}

void AGCUCharacter::Input_LeftMousePressed()
{

}

void AGCUCharacter::Input_RightMousePressed()
{
	// Dont do anything if we fall
	if (IsActorFalling())
		return;

	// Try to spawn our Grappling target

	if (GrapplingHook)
		GrapplingHook->SpawnGrapplingTarget();

}

void AGCUCharacter::Input_RightMouseReleased()
{
	// Dont do anything if we fall
	if (IsActorFalling())
		return;

	// Try to destroy our Grappling target

	if (GrapplingHook)
		GrapplingHook->DestroyGraplingTarget();
}

void AGCUCharacter::Input_RightMouseAxis(float inAxisValue)
{
	// Dont do anything if we fall
	if (IsActorFalling())
		return;

	// if inAxisValue != 0 that means we already pressed right mouse so we need update grappling target location
	if (inAxisValue != 0)
	{
		if (GrapplingHook)
			GrapplingHook->UpdateGrapplingTarget();
	}
}

void AGCUCharacter::Server_ActivateSuperJump_Implementation(float inJumpPower)
{
	// Get controll Rotation to get launch direction
	APlayerController* PC = Cast<APlayerController>(GetController());
	FVector LaunchDir = PC->GetControlRotation().Vector();

	// Add some upward direction always
	LaunchDir += FVector(0, 0, 1);

	LaunchDir *= inJumpPower;

	LaunchCharacter(LaunchDir, true, true);
}

bool AGCUCharacter::Server_ActivateSuperJump_Validate(float inJumpPower)
{
	return true;
}

bool AGCUCharacter::IsActorMoving()
{
	if (GetMovementComponent()->IsFalling())
		return false;

	if(GetVelocity().Equals(FVector(0, 0, 0), 0.f))
		return false;

	return true;
}

FVector AGCUCharacter::GetActorCurrentMovementVector()
{
	float ForwardValue = UKismetMathLibrary::MapRangeClamped(GetVelocity().X, GetMovementComponent()->GetMaxSpeed() * -1.f, GetMovementComponent()->GetMaxSpeed(), -1.f, 1.f);
	float SideValue = UKismetMathLibrary::MapRangeClamped(GetVelocity().Y, GetMovementComponent()->GetMaxSpeed() * -1.f, GetMovementComponent()->GetMaxSpeed(), -1.f, 1.f);
	return FVector(ForwardValue, SideValue, 0.f);
}

bool AGCUCharacter::IsActorFalling()
{
	return GetMovementComponent()->IsFalling();
}