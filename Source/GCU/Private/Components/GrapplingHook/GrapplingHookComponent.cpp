// Fill out your copyright notice in the Description page of Project Settings.

#include "GrapplingHookComponent.h"
#include "Characters/GCUCharacter.h"
#include "PhysicsEngine/PhysicsConstraintActor.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Core/Controller/GCUPlayerController.h"

UGrapplingHookComponent::UGrapplingHookComponent()
{
	MaxTargetDistance = 15000.f;

	bReplicates = true;

	InterpolationSpeed = 100.f;
}

void UGrapplingHookComponent::SpawnGrapplingTarget()
{
	if (!TargetActorClass)
		return;

	OwnerCharacter = Cast<AGCUCharacter>(GetOwner());
	
	if (!OwnerCharacter)
		return;

	OwnerPC = Cast<AGCUPlayerController>(OwnerCharacter->GetController());

	if (!OwnerPC)
		return;

	bool bValidTarget;

	FVector TargetLocation = GetLookAtLocation(bValidTarget);

	TargetActor = GetWorld()->SpawnActor<AGrapplingTarget>(TargetActorClass, TargetLocation, FRotator(0, 0, 0));

	UE_LOG(LogTemp, Warning, TEXT("Target Actor Spawned"));
}

void UGrapplingHookComponent::UpdateGrapplingTarget()
{
	// Just in case we do not spawned target actor
	if (!TargetActor)
		return;

	// Update Target Actor location
	bool bValidLocation;

	TargetActor->SetActorLocation(GetLookAtLocation(bValidLocation));
}

void UGrapplingHookComponent::DestroyGraplingTarget()
{
	if (!TargetActor)
		return;

	// Destroy Our Target Actor
	if (!TargetActor->IsPendingKill())
	{
		TargetActor->Destroy();
		TargetActor = nullptr;
	}



	// Create a new target actor in this place to check we hit something or just default distance
	bool bValidTarget;
	FVector TargetLocation = GetLookAtLocation(bValidTarget);

	TargetActor = GetWorld()->SpawnActor<AGrapplingTarget>(TargetActorClass, TargetLocation, FRotator(0, 0, 0));

	if(bValidTarget)
		Server_ActivateGrapplingHook(TargetActor->GetActorLocation());

	TargetActor->Destroy();

}

FVector UGrapplingHookComponent::GetLookAtLocation(bool& bFoundValidTarget)
{

	// start as we do not have valid target
	bFoundValidTarget = false;

	if (!OwnerCharacter || !OwnerPC)
		return FVector(0, 0, 0);

	// get our camera location
	FVector StartLocation = OwnerCharacter->CameraComponent->GetComponentLocation();

	// calculate our end location
	FVector EndLocation = StartLocation + (OwnerPC->GetControlRotation().Vector() * MaxTargetDistance);

	FHitResult HitResult;

	// If we hit something that means target actor location need modified by that hit point
	if (GetWorld()->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility))
	{
		bFoundValidTarget = true;
		return HitResult.ImpactPoint;
	}

	return EndLocation;
}

void UGrapplingHookComponent::Server_ActivateGrapplingHook_Implementation(FVector AtLocation)
{
	//SetWorldLocation(AtLocation);

	if (ActivatedTargetClass)
	{
		// Spawn our actor which handle constraint for us
		ActivatedTargetActor = GetWorld()->SpawnActor<AGrapplingTarget>(ActivatedTargetClass, AtLocation, FRotator(0, 0, 0));

		AGCUCharacter* OwnerCharacter = Cast<AGCUCharacter>(GetOwner());

		FTimerHandle GrapplingTimer;
		GetWorld()->GetTimerManager().SetTimer(GrapplingTimer, this, &UGrapplingHookComponent::TickGrapplingHook, GetWorld()->GetDeltaSeconds());
	}
}

void UGrapplingHookComponent::TickGrapplingHook()
{
	if (ActivatedTargetActor)
	{
		float Speed = GetWorld()->GetDeltaSeconds() * InterpolationSpeed;

		FVector NewLocation = (ActivatedTargetActor->GetActorLocation() - GetOwner()->GetActorLocation()) * Speed;

		AGCUCharacter* ownerChar = Cast<AGCUCharacter>(GetOwner());

		ownerChar->LaunchCharacter(NewLocation, true, true);

		float currentDistance = (ActivatedTargetActor->GetActorLocation() - ownerChar->GetActorLocation()).Size();

		// If we are end on grappling disable tick and destroy target actor
		if (GetOwner()->GetActorLocation().Equals(ActivatedTargetActor->GetActorLocation(), 100.f) || currentDistance <= 500.f)
		{
			ActivatedTargetActor->Destroy();
			ActivatedTargetActor = nullptr;

			return;
		}

		FTimerHandle GrapplingTimer;
		GetWorld()->GetTimerManager().SetTimer(GrapplingTimer, this, &UGrapplingHookComponent::TickGrapplingHook, GetWorld()->GetDeltaSeconds());
	}
}

bool UGrapplingHookComponent::Server_ActivateGrapplingHook_Validate(FVector AtLocation)
{
	return true;
}