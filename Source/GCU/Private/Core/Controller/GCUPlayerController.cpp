// Fill out your copyright notice in the Description page of Project Settings.

#include "GCUPlayerController.h"
#include "UI/MainCharacter/MainCharacterWidget.h"
#include "Characters/GCUCharacter.h"

void AGCUPlayerController::Possess(APawn* aPawn)
{
	Super::Possess(aPawn);

	AGCUCharacter* Pawn = Cast<AGCUCharacter>(aPawn);

	if (Pawn)
		Client_AddMainCharacterWidget();
}

void AGCUPlayerController::Client_AddMainCharacterWidget_Implementation()
{
	AGCUCharacter* OwnerCharacter = Cast<AGCUCharacter>(GetPawn());

	if (!OwnerCharacter)
		return;

	if (MainCharacterWidgetClass)
	{
		MainCharacterWidget = CreateWidget<UMainCharacterWidget>(this, MainCharacterWidgetClass);
		
		MainCharacterWidget->AddToViewport(1);
		MainCharacterWidget->OnWidgetAdded(OwnerCharacter);
	}
}


