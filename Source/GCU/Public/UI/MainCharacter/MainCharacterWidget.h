// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MainCharacterWidget.generated.h"

/**
 * 
 */
UCLASS()
class GCU_API UMainCharacterWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UPROPERTY(BlueprintReadOnly, Category = "Main Character Widget")
	class AGCUCharacter* OwnerCharacter;

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = "Main Character Widget")
	class AGCUCharacter* GetCastedOwner() { return OwnerCharacter; }

	void OnWidgetAdded(class AGCUCharacter* inOwnerCharacter);
	
	
};
