// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Characters/GCUBaseCharacter.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "GCUCharacter.generated.h"

/**
 * 
 */
UCLASS()
class GCU_API AGCUCharacter : public AGCUBaseCharacter
{
	GENERATED_BODY()
	
public:

	AGCUCharacter();

	// Our default camera component
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Camera Component", meta = (AllowPrivateAccess = "true"))
	UCameraComponent* CameraComponent;
	
	// Spring arm component
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Camera Spring Arm", meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* SpringArm;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Physics Constraint", meta = (AllowPrivateAccess = "true"))
	class UGrapplingHookComponent* GrapplingHook;

	/************************************************************************/
	/* Character Interfaces and Inherited UObject functions                 */
	/************************************************************************/
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void Tick(float DeltaTime) override;


	/************************************************************************/
	/* Input Functions                                                      */
	/************************************************************************/

public:

	bool bInvertMouseEnabled;

	// Handling Forward and Backward movement
	void Input_MoveForwardBack(float inAxisValue);

	// Handling Left and Right Movement
	void Input_MoveLeftRight(float inAxisValue);

	// Handling Mouse Pitch
	void Input_MousePitch(float inAxisValue);

	// Handling Mouse Yaw
	void Input_MouseYaw(float inAxisValue);

	// Handling Jump
	void Input_JumpPressed();

	// Handling JumpRelease
	void Input_JumpReleased();

	// Handling Left Mouse
	void Input_LeftMousePressed();

	// Handling Right Mouse
	void Input_RightMousePressed();

	// Handling Right Mouse Released
	void Input_RightMouseReleased();

	// Grappling hook target update
	void Input_RightMouseAxis(float inAxisValue);

	/************************************************************************/
	/* Input Functions End                                                  */
	/************************************************************************/

	/* Super Jump */
	
	// Store our current SuperJump Value
	bool bSuperJump;

	UPROPERTY(BlueprintReadWrite, Category = "Super Jump")
	float SuperJumpPower;

	// Activate Super Jump
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_ActivateSuperJump(float inJumpPower);

	// Multiplier value when space is pressed we multiply this value with delta to get super Power
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Character Special Ability Configs")
	float SuperJumpPowerMultiplier;

	/************************************************************************/
	/* Movement and AnimBp                                                  */
	/************************************************************************/
	UFUNCTION(BlueprintPure, BlueprintCallable,  Category = "Character Movement")
	bool IsActorMoving();

	UFUNCTION(BlueprintPure, BlueprintCallable,  Category = "Character Movement")
	FVector GetActorCurrentMovementVector();

	UFUNCTION(BlueprintPure, BlueprintCallable,  Category = "Character Movement")
	bool IsActorFalling();
	
};
