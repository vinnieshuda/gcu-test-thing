// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GCUPlayerController.generated.h"

// Forward
class UMainCharacterWidget;

UCLASS()
class GCU_API AGCUPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:

	/* APlayer Controller interfaces */

	virtual void Possess(APawn* aPawn) override;
	
	/* APlayerController interfaces end */

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Character UI")
	TSubclassOf<UMainCharacterWidget> MainCharacterWidgetClass;

	UPROPERTY(BlueprintReadOnly, Category = "Character UI")
	UMainCharacterWidget* MainCharacterWidget;

	UFUNCTION(Client, Reliable)
	void Client_AddMainCharacterWidget();
};
