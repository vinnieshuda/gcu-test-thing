// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "Actors/GrapplingTarget/GrapplingTarget.h"
#include "Actors/GrapplingHelper/GrapplingHelperActor.h"
#include "GrapplingHookComponent.generated.h"
 
UCLASS()
class GCU_API UGrapplingHookComponent : public USceneComponent
{
	GENERATED_BODY()
	
public:

	UGrapplingHookComponent();

	UPROPERTY()
	AGrapplingTarget* TargetActor;

	UPROPERTY()
	AGrapplingTarget* ActivatedTargetActor;

	class APhysicsConstraintActor* GraplingHelper;

	UPROPERTY()
	class AGCUCharacter* OwnerCharacter;

	UPROPERTY()
	class AGCUPlayerController* OwnerPC;

	// Spawn Grappling targeting actor at given location
	void SpawnGrapplingTarget();

	// Update Grappling target location
	void UpdateGrapplingTarget();

	// Destroy Actor
	void DestroyGraplingTarget();

	// Returning a world location based on controll rotation and max target distance
	FVector GetLookAtLocation(bool& bFoundValidTarget);

	// Activating Grappling hook at given location
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_ActivateGrapplingHook(FVector AtLocation);

	void TickGrapplingHook();

	/************************************************************************/
	/* Grappling Configuration                                              */
	/************************************************************************/

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grappling Hook Configuration")
	float MaxTargetDistance;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grappling Hook Configuration")
	float InterpolationSpeed;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grappling Hook Configuration")
	TSubclassOf<AGrapplingTarget> TargetActorClass;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grappling Hook Configuration")
	TSubclassOf<AGrapplingTarget> ActivatedTargetClass;

};
